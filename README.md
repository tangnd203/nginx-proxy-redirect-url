# Nginx Proxy Redirect URLs
## 1 - Prerequisites
- proxy : 192.168.188.132
- webserver1 : 192.168.188.130
- webserver 2 : 192.168.188.131
- client : 10.0.0.124
- file abc.html
- installed nginx
## 2 - Configure
- add hosts
```
    $ sudo vim /etc/hosts
```
```bash
    192.168.188.100 wss.vn
```
- config redirect
```
    $ sudo vim /etc/nginx/sites-enabled/default
```
```bash
    server {
            listen 80 default_server;
            listen [::]:80 default_server;
    
            root /var/www/html;
    
            # Add index.php to the list if you are using PHP
            index index.html index.htm index.nginx-debian.html;
    
            server_name wss.vn;
    
            location ~ ^/(.*)$ {
    
                    if ($args != "") {
                            return 302 /$1;
                    }
                    proxy_pass http://backend/$1;
            }
    
            location / {
    
                    if ($args != "") {
                            return 302 /;
                    }
                    proxy_pass http://backend;
            }
    }
```
- location ~ ^/(.*)$ { ... }
  + ~ : so khớp với biểu thức chính quy
  + ^ : dấu bắt đầu trong biểu thức chính quy
  + /(.*) : khớp với bất kỳ URI nào 
  + $ : dấu kết thúc trong biểu thức chính quy 
  + $1 : phần URI được khớp với phần (.*)

## 3 - Test 
- wss.vn
![alt text](images/image1.png)
- wss.vn?abcjs
![alt text](images/image2.png)
- wss.vn/abc.html
![alt text](images/image3.png)
- wss.vn/abc.html?adbc=jmvm
![alt text](images/image4.png)
- wss.vn/test.txt
![alt text](images/image5.png)
- wss.vn/test.txt?page=1
![alt text](images/image6.png)
- wss.vn/abc.html/?page=1
![alt text](images/image7.png)

## 4 - Conclude
- tại sao lại redirect url?
  + trong trường hợp này cấu hình redirect để chuyển hướng những request ko hợp lệ của người dùng 